/*
This file is part of gnucap-adms

Copyright (C) 2018 Felix Salfelder <felix@salfelder.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`ifdef DISCIPLINE_H
`else
`define DISCIPLINE_H 1

`include "disciplines.vams"

nature Damage
  units = "1";
  access = State;
  abstol = 0.;
endnature
nature Stress
  units = "1";
  access = Level;
  abstol = 0.;
endnature
discipline degradational
  potential Damage;
  flow Stress;
enddiscipline

nature concentration
  access = C;
  units = "%"; // percentage
  abstol = 1.e-5;
endnature
nature reaction
  access = R;
  units = "W"; // whatever
  abstol = 1e-8;
endnature
discipline chemical
  potential concentration;
  flow reaction;
enddiscipline

nature Magnetos
  units = "yes";
  access = MMF;
  abstol = 17.;
endnature
nature Fluxi
  units = "..";
  access = Psi;
  abstol = 19.;
endnature
discipline magnetic_
  potential Magnetos;
  flow Fluxi;
enddiscipline

nature Angular_Flow
  units = "angels";
  access = uaT;
  abstol = 42.5;
endnature
nature Angular_Speed
  units = "spinners";
  access = Omega;
  abstol = 42.;
endnature
discipline rotational_w
  potential Angular_Speed;
  flow Angular_Flow;
enddiscipline

`endif
